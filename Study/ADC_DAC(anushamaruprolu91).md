## ADC-Analog to Digital Converter

An electronic integrated circuit which transforms a signal from analog(continuous) form to digital(discrete) form is known as ADC.

Analog signals are directly measurable quantities while digital signals only have two states(binary 0 and binary 1).

![ADC](https://www.rfwireless-world.com/images/ADC.jpg)

Purpose of Analog to Digital conversion:
- Processors perform arithmatic operations on digital signals.
- Signals in digital form are less susceptible to effects of noise.
- ADC provides link between analog world (of transducers) and digital world (of data and signal processing).

* Following are the types of Analog to Digital Converter(ADC):
1. Counter type(simplest)
2. Successive Approximation
3. Flash ADC
4. Sigma Delta

# Applications:
1. Digital Voltmeter: measures voltage in analog and convert to digital form using ADC for digital representation form.
2. Mobile phone: Analog voice is converted to digital form for further processing(speech compression,encoding etc.) before it is converted back to analog form for transmission.
3. Scanner: When we take photo, the scanner uses ADC internally to convert analog information provided by picture into digital information.
4. Voice Recorder: It uses ADC to convert analog voice information to the digital information. Latest VOIP solutions utilize the same concept.

## DAC-Digital to Analog Converter:

To convert digital values to analog voltage DAC is used. It performs inverse operation of analog to digital converter.

Analog output = ( Digital Input/(2N -1) )* Reference Input

![DAC](https://www.rfwireless-world.com/images/DAC.jpg)

There are two types of DACs:
1. Weighted Resistor
2. Resistive Divider

Following specifications are considered for the design, development as well as selection of DAC(Digital to Analog Converter).
- Resolution
- Reference voltages
- Settling Time
- Linearity
- Speed
- Errors

# Applications:
1. Modems need DAC to convert data to analog form so that it can be carried over telephone wires.
2. Video adapters also need DACs known as RAMDACs to convert digital form of data to analog form.
3. Digital Motor Control
4. Printers
5. Sound Equipments
6. Function Generators or Oscilloscopes
7. Digital Audio

## Difference Between ADCs and DACs:

In simplest terms,an ADC is attempting to capture and convert a largely unknown signal into a known representation. In contrast, a DAC is taking a fully known, well-understood representation and “simply” generating an equivalent analog value.

The two basic converter types, analog-to-digital converters (ADCs) and digital-to-analog converters (DACs), share many common circuit elements and even come in similar multi-pin packages. Key performance parameters are identical between the two, including clock speed, sampling rates, and bandwidth, but they differ in many other ways. 

![](https://th.bing.com/th/id/OIP.ZGRNVoJvdG61G3I7TOqdqQHaEK?w=318&h=180&c=7&o=5&dpr=1.25&pid=1.7)


## Raspberry pi

Raspberry pi is a powerful palm sized pocket computer based on the ARM cortex architecture. Because of its high operating speed, memory and wireless capabilities it is used by electronics engineers and makers for a variety of applications like Home Automation, Internet of Things (IoT), Machine learning, Computer vision etc.. 

![](https://th.bing.com/th/id/OIP.qqtVnd8NwCvhzJ2_hPbdnQHaEW?w=285&h=180&c=7&o=5&dpr=1.25&pid=1.7)

Normal controllers have ADC channels but for PI there are no ADC channels provided internally. So if we want to interface any analog sensors we need an ADC conversion unit. 

Microcontrollers work only with digital values but in real world we have to deal with Analog signals. That’s why ADC (Analog to Digital Converters) is there to convert real world Analog values into Digital form so that microcontrollers can process the signals. But what if we need Analog signals from digital values, so here comes the DAC (Digital to Analog Converter).

# Interfacing PCF8591 ADC/DAC Module with Raspberry Pi

Circuit diagram for Interfacing of PCF8591 with Raspberry Pi is simple.we will read the analog values from any of the analog pins and show it on Raspberry Pi terminal. We can change the values using a 100K pot.

![](https://circuitdigest.com/sites/default/files/circuitdiagram_mic/Circuit-Diagram-for-PCF8591-ADC-with-Raspberry-Pi.png)
