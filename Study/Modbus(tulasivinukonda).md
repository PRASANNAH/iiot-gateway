## MODBUS

Modbus is a data communications protocol originally published by Modicon (now Schneider Electric) in 1979 for use with its programmable logic controllers (PLCs). Modbus has become a de facto standard communication protocol and is now a commonly available means of connecting industrial electronic devices.Modbus is popular in industrial environments because it is openly published and royalty-free. It was developed for industrial applications, is relatively easy to deploy and maintain compared to other standards, and places few restrictions - other than the datagram (packet) size - on the format of the data to be transmitted. Modbus uses the RS485 or Ethernet as its wiring type. Modbus supports communication to and from multiple devices connected to the same cable or Ethernet network. For example, a device that measures temperature and a different device to measure humidity, both of which communicates the measurements to a computer.

Modbus is often used to connect a plant/system supervisory computer with a remote terminal unit (RTU) in Supervisory Control and Data Acquisition (SCADA) systems in the electric power industry. Many of the data types are named from industrial control of factory devices, such as Ladder logic because of its use in driving relays: A single physical output is called a coil, and a single physical input is called a discrete input or a contact. 

## LIMITATIONS

1. Since Modbus was designed in the late 1970s to communicate to programmable logic controllers, the number of data types is limited to those understood by PLCs at the time. Large binary objects are not supported.
2. No standard way exists for a node to find the description of a data object, for example, to determine whether a register value represents a temperature between 30 and 175 degrees.
3. Since Modbus is a master/slave protocol, there is no way for a field device to "report an exception" (except over Ethernet TCP/IP, called open-mbus) – the master node must routinely poll each field device and look for changes in the data. This consumes bandwidth and network time in applications where bandwidth may be expensive, such as over a low-bit-rate radio link.
4. Modbus is restricted to addressing 247 devices on one data link, which limits the number of field devices that may be connected to a master station (once again, Ethernet TCP/IP is an exception).
5. Modbus transmissions must be contiguous, which limits the types of remote communications devices to those that can buffer data to avoid gaps in the transmission.
6. Modbus protocol itself provides no security against unauthorized commands or interception of data.

## MODBUS OBJECT TYPES:

Coil 	     :  Read-write  	1 bit   	00001 - 09999

Discrete input:	Read-only   	1 bit   	10001 - 19999

Input register:	Read-only   	16 bits         30001 - 39999

Holding register:Read-write      16 bits           40001 - 49999 

## VERSIONS OF MODBUS PROTOCOL

1. Modbus RTU — This is used in serial communication and makes use of a compact, binary representation of the data for protocol communication. The RTU format follows the commands/data with a cyclic redundancy check checksum as an error check mechanism to ensure the reliability of data. Modbus RTU is the most common implementation available for Modbus. A Modbus RTU message must be transmitted continuously without inter-character hesitations. Modbus messages are framed (separated) by idle (silent) periods.
2. Modbus ASCII — This is used in serial communication and makes use of ASCII characters for protocol communication. The ASCII format uses a longitudinal redundancy check checksum. Modbus ASCII messages are framed by leading colon (":") and trailing newline (CR/LF).
3.  Modbus TCP/IP or Modbus TCP — This is a Modbus variant used for communications over TCP/IP networks, connecting over port 502. It does not require a checksum calculation, as lower layers already provide checksum protection.
4. Modbus over TCP/IP or Modbus over TCP or Modbus RTU/IP — This is a Modbus variant that differs from Modbus TCP in that a checksum is included in the payload as with Modbus RTU.
5. Modbus over UDP — Some have experimented with using Modbus over UDP on IP networks, which removes the overheads required for TCP.
6. Modbus Plus (Modbus+, MB+ or MBP) — Modbus Plus is proprietary to Schneider Electric and unlike the other variants, it supports peer-to-peer communications between multiple masters.It requires a dedicated co-processor to handle fast HDLC-like token rotation. It uses twisted pair at 1 Mbit/s and includes transformer isolation at each node, which makes it transition/edge-triggered instead of voltage/level-triggered. Special hardware is required to connect Modbus Plus to a computer, typically a card made for the ISA, PCI or PCMCIA bus.
7. Pemex Modbus — This is an extension of standard Modbus with support for historical and flow data. It was designed for the Pemex oil and gas company for use in process control and never gained widespread adoption.
8. Enron Modbus — This is another extension of standard Modbus developed by Enron Corporation with support for 32-bit integer and floating-point variables and historical and flow data. Data types are mapped using standard addresses.The historical data serves to meet an American Petroleum Institute (API) industry standard for how data should be stored.

## COMMANDS 
Modbus commands can instruct a Modbus Device to:

change the value in one of its registers, that is written to Coil and Holding registers.
read an I/O port: Read data from a Discrete and Coil ports,command the device to send back one or more values contained in its Coil and Holding registers.

A Modbus command contains the Modbus address of the device it is intended for (1 to 247). Only the addressed device will respond and act on the command, even though other devices might receive it (an exception is specific broadcastable commands sent to node 0, which are acted on but not acknowledged.

All Modbus commands contain checksum information to allow the recipient to detect transmission errors. 



